# Levels and Tags

Similar to the esp_log module in the esp32 IDF, the library implements a modular leveling system with tags.

It is best practice to use a tag per module/file/class.

```c
static const char* tag = "module";

// Use tag in all the module's related log
log_i(tag, "Hello World!");
```

## Compile time

You can enable/disable all logs using the `LOG_ENABLED` macro. This can be used to add/remove all logs from Debug/Production builds.

```cmake
# CMakeLists.txt
target_compile_definitions(${your_app} PUBLIC "-DLOG_ENABLED=0") # Disable all logs
```

The default master level can also be set using `LOG_DEFAULT_LEVEL` macro. (default: `LOG_LEVEL_INFO`).


You can set the maximum level of logs that should be compiled with `LOG_MAXIMUM_LEVEL`. By default, it is the same as `LOG_DEFAULT_LEVEL` above.


At a file level, you can set the level of logs that should be compiled by defining `LOG_LOCAL_LEVEL` before including `logger.h`.

```c
#define LOG_LOCAL_LEVEL LOG_LEVEL_VERBOSE
#include "logger.h"
```


## Runtime

### Master Level

Logging output can be completely enabled and disabled at runtime:

```c
logger__disable();

// Can retrieve state at anytime using
bool enabled = logger__is_enabled();

logger__enable();
```

The global output level of the library can also be set at runtime. It is initially set to `LOG_DEFAULT_LEVEL`.

```c
logger__set_level(ESP_LOG_DEBUG); // Enforce output to DEBUG
```

You can also call `logger__get_level()` to get the current log level.

Notes:
> 1. Any logs with a level superior to the global level will not be forwarded to the [handlers](./handlers.md).
> 2. The master level is preserved between `logger__disable()` and `logger__enable()` calls.
> 2. It is recommended to only use this function in a top level application/module and not in individual modules or libraries as this will override any tag level set by the user elsewhere.



### Individual Tag Levels

Each tag's level can be configured at runtime. A wildcard "\*" is supported and will set all tags.

```c
logger__set_tag_level("*", ESP_LOG_DEBUG);   // set all tags to DEBUG
logger__set_tag_level("main", ESP_LOG_INFO); // set logs with the "main" tag to INFO
```

When a tag's level has not been explicitely set, it's level will be set to `LOG_DEFAULT_LEVEL` when logged for the first time.

Notes:
> 1. After a wildcard call, any subsequent call to `logger__set_tag_level()` will override the value for the tag. 
> 2. It is more efficient to set the master level with `logger__set_level()` to enforce a global log level instead of a wildcard call to `#!c logger__set_tag_level("*", ...)` as the master log level is checked before retrieving a tag's level.
