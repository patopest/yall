# Yet Another Logging Library (YALL)

A logging library written in C for both Software and Firmware.


## Supported Platforms

- Operating Systems:
    - macOS
    - Linux
    - Windows
- Embedded:
    - RPI Pico (rp2040 MCU) using the [pico-sdk](https://github.com/raspberrypi/pico-sdk)
    - FreeRTOS (TBA)
    - Direct ('No OS')
