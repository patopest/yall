# Getting Started


## Integrations in your project

### CMake

#### External

If the library is installed on your system you can locate it from a CMake project with `find_package()` in your `CMakeLists.txt` file:

```cmake
...
find_package(yall 0.3.0 REQUIRED)
target_link_libraries(foo PRIVATE yall)
...
```

**NOTE:** This approach only works for a project built on the same host as the target project. (ie: macOS, Windows, Linux).

#### Internal

To embed this library directly in you project, clone/download the source tree in a subdirectory and call `add_subdirectory()` in your `CMakeLists.txt` file:

```cmake
...
add_subdirectory(yall)
target_link_libraries(foo PRIVATE yall)
...
```

**NOTE:** This is the required approach if including the library in a Firmware project as the project will cross-compile for your target MCU.

### Others

TBA



## Usage

Once the library is integrated you can start using it.

```c
#include "logger.h"

logger__init();
logger__set_level(LOG_LEVEL_INFO); // optionally set global logging level

log_info("main", "Hello World!");

```

This will create a default console handler and log to stdout.  
See [Log Handlers](handlers.md) for more information about the different types of lag handlers and how to use them.


### Logging Levels

There are currently 5 levels supported:

- Error:    `LOG_LEVEL_ERROR` (lowest)
- Warning:  `LOG_LEVEL_WARNING`
- Info:     `LOG_LEVEL_INFO`
- Debug:    `LOG_LEVEL_DEBUG`
- Verbose:  `LOG_LEVEL_VERBOSE` (highest)


The logging functions can be accessed in multiple ways:

```c
/* Wrappers for each log levels */
log_error(tag, message);
log_warning(tag, message);
log_info(tag, message);
log_debug(tag, message);
log_verbose(tag, message);

log_e(tag, message); // alias to log_error()
log_w(tag, message); // alias to log_warning()
log_i(tag, message); // alias to log_info()
log_d(tag, message); // alias to log_debug()
log_v(tag, message); // alias to log_verbose()

LOGE(tag, message); // alias to log_error()
LOGW(tag, message); // alias to log_warning()
LOGI(tag, message); // alias to log_info()
LOGD(tag, message); // alias to log_debug()
LOGV(tag, message); // alias to log_verbose()

/* Explicitely specifying the level */
LOG(LOG_LEVEL_INFO, tag, message);
log(LOG_LEVEL_INFO, tag, message); // alias to LOG()
```

See [Levels and Tags](levels-and-tags.md) for more information about the tag system and level control.

