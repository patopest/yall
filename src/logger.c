#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <time.h>
#include <string.h>

#include "logger.h"
#include "logger_tags.h"
#include "logger_handlers.h"
#include "platform.h"


/* --------- Local Variables --------- */
static bool initialized = false;
static bool enabled = LOG_ENABLED;
static log_level_t global_level = LOG_DEFAULT_LEVEL;
static logger_handler_t handlers[LOG_MAX_HANDLERS];
static uint8_t num_handlers = 0;
static char time_str[LOG_TIMESTAMP_TOTAL_SIZE];



void logger__init() {

    if (!initialized) {
        platform_lock_init();

        // Add a default handler
        if (num_handlers == 0) {
            logger__add_handler(logger__handler_get_default());
        }
        initialized = true;
    }
    logger__init_handlers();

}


void logger__enable() {
    enabled = true;
}


void logger__disable() {
    enabled = false;
}


bool logger__is_enabled() {
    return enabled;
}


void logger__set_level(log_level_t level) {
    global_level = level;
}


log_level_t logger__get_level() {
    return global_level;
}


// Windows only implementation as gmtime_r does not exist
#if defined(_WIN32) || defined(_WIN64)
static struct tm* gmtime_r(const time_t* time, struct tm* result)
{
    gmtime_s(result, time);
    return result;
}
#endif


char *logger__timestamp(const char* format, uint8_t subseconds) {

    struct timespec time = platform_time();
    struct tm calendar;
    // localtime_r(&time.tv_sec, &calendar); // system localtime
    gmtime_r(&time.tv_sec, &calendar);       // UTC time

    uint8_t size = strftime(time_str, LOG_TIMESTAMP_FORMAT_SIZE, format, &calendar);
    if (size == 0) {
        fprintf(stderr, "Error: Size of string for strftime() timestamp is too short. Please increase LOG_TIMESTAMP_FORMAT_SIZE, currently = %d\n", LOG_TIMESTAMP_FORMAT_SIZE);
    }

    uint16_t index;
    if (subseconds > 0) {
        index = strlen(time_str);
    }
    if (subseconds == 1) {
        sprintf(&time_str[index], ".%03ld", time.tv_nsec / 1000000);
    }
    else if (subseconds == 2) {
        sprintf(&time_str[index], ".%06ld", time.tv_nsec / 1000);
    }
    else if (subseconds == 3) {
        sprintf(&time_str[index], ".%09ld", time.tv_nsec);
    }

    return time_str;
}


void logger__write(log_level_t level, const char *tag, const char *format, ...) {
    va_list args;

    if (initialized == false) {
        fprintf(stderr, "Error: Logger is not initialized. Call logger__init() first\n");
        return;
    }
    if (enabled == false || level > global_level) {
        return;
    }
    if (level > logger__get_tag_level(tag)) {
        return;
    }

    platform_lock();

    for (uint8_t i = 0; i < num_handlers; i++) {
        va_start(args, format);
        handlers[i].write(&handlers[i].context, format, args);
        va_end(args);
    }
    
    platform_unlock();
}


void logger__init_handlers() {

    platform_lock();
    
    for (uint8_t i = 0; i < num_handlers; i++) {
        handlers[i].init(&handlers[i].context);
    }
    
    platform_unlock();
}


void logger__add_handler(logger_handler_t handler) {

    if (handler.name == NULL) {
        fprintf(stderr, "Error: cannot add a log handler without a name\n");
        return;
    }

    if (logger__get_handler(handler.name) != NULL) {
        fprintf(stderr, "Error: handler name already exists\n");
        return;
    }

    platform_lock();

    if (num_handlers < LOG_MAX_HANDLERS) {
        handlers[num_handlers++] = handler;
    }
    else {
        fprintf(stderr, "Error: max handlers reached!");
    }

    platform_unlock();
}


void logger__remove_handlers() {

    platform_lock();
    
    num_handlers = 0;
    memset(handlers, 0, sizeof(handlers));
    
    platform_unlock();
}


void logger__flush_handlers() {

    platform_lock();

    for (uint8_t i = 0; i < num_handlers; i++) {
        handlers[i].flush(&handlers[i].context);
    }

    platform_unlock();
}


bool logger__remove_handler(const char* name) {

    platform_lock();

    bool found = false;
    for (uint8_t i = 0; i < num_handlers; i++) {
        if (strcmp(handlers[i].name, name) == 0) {
            found = true;
        }
        if ((found == true) && (i + 1 < LOG_MAX_HANDLERS)) {
            handlers[i] = handlers[i + 1];
        }
    }
    if (found == true) {
        num_handlers--;
    }

    platform_unlock();
    return found;
}


logger_handler_t *logger__get_handler(const char *name) {

    logger_handler_t *handler = NULL;
    for (uint8_t i = 0; i < num_handlers; i++) {
        if (strcmp(handlers[i].name, name) == 0) {
            handler = &handlers[i];
        }
    }

    return handler;
}


// Wrapper on handlers
void logger__handler_set_print_function(const char *name, vfprintf_t print_func) {

    logger_handler_t *handler = logger__get_handler(name);

    platform_lock();
    if (handler != NULL) {
        switch (handler->type) {
            case LOGGER_HANDLER_CONSOLE:
                handler->context.console.print = print_func;
                break;
            case LOGGER_HANDLER_FILE:
                handler->context.file.print = print_func;
                break;
            case LOGGER_HANDLER_ROTATINGFILE:
                handler->context.rotating_file.print = print_func;
                break;
            default:
                break;
        }
    }

    platform_unlock();
}

// Wrapper on file loggers
void logger__handler_set_filename(const char *name, char *filename) {

    logger_handler_t *handler = logger__get_handler(name);

    platform_lock();
    if (handler != NULL) {
        switch (handler->type) {
            case LOGGER_HANDLER_FILE:
                handler->context.file.filename = filename;
                break;
            case LOGGER_HANDLER_ROTATINGFILE:
                handler->context.rotating_file.filename = filename;
                break;
            default:
                break;
        }
        handler->init(&(handler->context)); // Reinit to open new file
    }    

    platform_unlock();
}

