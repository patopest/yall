#ifndef LOGGER_TAGS_H
#define LOGGER_TAGS_H


#include "logger.h"


void logger__set_tag_level(const char *tag, log_level_t level);
log_level_t logger__get_tag_level(const char *tag);


#endif