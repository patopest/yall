#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>

#include "logger_handlers.h"
#include "logger.h"


/* --------- Local Defintions --------- */
#define DEFAULT_CONSOLE_NAME    "console"
#define DEFAULT_CONSOLE_STREAM  stdout
#define ROTATING_FILE_EXT_SIZE  5


/* --------- Local Functions --------- */
static void console_init(logger_handler_context_t *ctx);
static void console_write(logger_handler_context_t *ctx, const char *format, va_list args);
static void console_flush(logger_handler_context_t *ctx);
static void file_init(logger_handler_context_t *ctx);
static void file_write(logger_handler_context_t *ctx, const char *format, va_list args);
static void file_flush(logger_handler_context_t *ctx);
static void rotating_file_init(logger_handler_context_t *ctx);
static void rotating_file_write(logger_handler_context_t *ctx, const char *format, va_list args);
static void rotating_file_flush(logger_handler_context_t *ctx);
static uint32_t get_file_size(FILE *fp);
static bool file_exists(const char* filename);
static void get_backup_filename(const char* filename, uint8_t index, char *backup_name, size_t size);



logger_handler_t logger__handler_get_default() {
    return logger__handler_get_console(DEFAULT_CONSOLE_NAME, NULL);
}


/* --------- Console Handler Functions --------- */
logger_handler_t logger__handler_get_console(const char *name, FILE *stream) {

    logger_handler_t handler = {
        .name = name,
        .type = LOGGER_HANDLER_CONSOLE,
        .init = console_init,
        .write = console_write,
        .flush = console_flush,
        .context.console = { 
            .file = DEFAULT_CONSOLE_STREAM,
            .print = &vfprintf,
        },
    };
    if (stream != NULL) {
        handler.context.console.file = stream;
    }

    return handler;
}


static void console_init(logger_handler_context_t *ctx) {

    if (ctx->console.file != stdout && ctx->console.file != stderr) {
        fprintf(stderr, "Error: Stream for logger console handler must be stdout or stderr");
        return;
    }
}


static void console_write(logger_handler_context_t *ctx, const char *format, va_list args) {

    ctx->console.print(ctx->console.file, format, args);
}


static void console_flush(logger_handler_context_t *ctx) {

    fflush(ctx->console.file);
}


/* --------- File Handler Functions --------- */
logger_handler_t logger__handler_get_file(const char *name, char *filename) {

    logger_handler_t handler = {
        .name = name,
        .type = LOGGER_HANDLER_FILE,
        .init = file_init,
        .write = file_write,
        .flush = file_flush,
        .context.file = { 
            .file = NULL,
            .filename = filename,
            .print = &vfprintf,
        },
    };

    return handler;
}


static void file_init(logger_handler_context_t *ctx) {

    if (ctx->file.filename == NULL) {
        fprintf(stderr, "Error: filename for logger file handler must not be NULL");
        return;
    }

    if (ctx->file.file != NULL) {
        fflush(ctx->file.file);
        fclose(ctx->file.file);
    }
    ctx->file.file = fopen(ctx->file.filename, "a");
    if (ctx->file.file == NULL) {
        fprintf(stderr, "Error: Unable to open file %s", ctx->file.filename);
    }

}


static void file_write(logger_handler_context_t *ctx, const char *format, va_list args) {

    if (ctx->file.file == NULL) {
        fprintf(stderr, "Error: file %s is not open, please init handler first by calling logger__init()\n", ctx->file.filename);
        return;
    }
    ctx->file.print(ctx->file.file, format, args);
}


static void file_flush(logger_handler_context_t *ctx) {

    fflush(ctx->file.file);
}


/* --------- Rotating File Handler Functions --------- */
logger_handler_t logger__handler_get_rotating_file(const char *name, char *filename, uint32_t max_file_size, uint8_t backup_count) {

    logger_handler_t handler = {
        .name = name,
        .type = LOGGER_HANDLER_ROTATINGFILE,
        .init = rotating_file_init,
        .write = rotating_file_write,
        .flush = rotating_file_flush,
        .context.rotating_file = { 
            .file = NULL,
            .filename = filename,
            .print = &vfprintf,
            .file_size = 0,
            .max_file_size = max_file_size,
            .backup_count = backup_count,
        },
    };

    return handler;
}


static void rotating_file_init(logger_handler_context_t *ctx) {

    if (ctx->rotating_file.filename == NULL) {
        fprintf(stderr, 
            "Error: filename for logger file handler must not be NULL");
        return;
    }

    if (ctx->rotating_file.file != NULL) {
        fflush(ctx->rotating_file.file);
        fclose(ctx->rotating_file.file);
    }
    ctx->rotating_file.file = fopen(ctx->rotating_file.filename, "a");
    if (ctx->rotating_file.file == NULL) {
        fprintf(stderr, "Error: Unable to open file %s", ctx->rotating_file.filename);
        return;
    }
    ctx->rotating_file.file_size = get_file_size(ctx->rotating_file.file);

}


static void rotating_file_write(logger_handler_context_t *ctx, const char *format, va_list args) {

    if (ctx->rotating_file.file == NULL) {
        fprintf(stderr, "Error: file %s is not open, please init handler first by calling logger__init()\n", ctx->rotating_file.filename);
        return;
    }
    int size = ctx->rotating_file.print(ctx->rotating_file.file, format, args);
    ctx->rotating_file.file_size += size;

    // Rotate if necessary
    if (ctx->rotating_file.file_size >= ctx->rotating_file.max_file_size) {
        char *filename = ctx->rotating_file.filename;
        size_t size = strlen(filename);
        char *src_filename = (char *) malloc(size + ROTATING_FILE_EXT_SIZE);
        char *dst_filename = (char *) malloc(size + ROTATING_FILE_EXT_SIZE);

        fflush(ctx->rotating_file.file);
        fclose(ctx->rotating_file.file);
        for (uint8_t i = ctx->rotating_file.backup_count; i > 0; i--) {
            get_backup_filename(filename, i - 1, src_filename, size + ROTATING_FILE_EXT_SIZE);
            get_backup_filename(filename, i, dst_filename, size + ROTATING_FILE_EXT_SIZE);

            if (file_exists(dst_filename)) {
                if (remove(dst_filename) != 0) {
                    fprintf(stderr, "Error: Removing backup file %s\n", dst_filename);
                }
            }
            if (file_exists(src_filename)) {
                if (rename(src_filename, dst_filename) != 0) {
                    fprintf(stderr, "Error: Renaming backup file %s -> %s\n", src_filename, dst_filename);
                }
            }
        }
        free(src_filename);
        free(dst_filename);

        // Re-open new file
        ctx->rotating_file.file = fopen(ctx->rotating_file.filename, "a");
        if (ctx->rotating_file.file == NULL) {
            fprintf(stderr, "Error: Unable to open file %s", ctx->rotating_file.filename);
            return;
        }
        ctx->rotating_file.file_size = get_file_size(ctx->rotating_file.file);
    }
}


static void rotating_file_flush(logger_handler_context_t *ctx) {

    fflush(ctx->rotating_file.file);
}


static uint32_t get_file_size(FILE *fp) {
    uint32_t size = 0;
    fseek(fp, 0, SEEK_END);
    size = ftell(fp);
    return size;
}


static bool file_exists(const char* filename) {
    FILE *fp;

    if ((fp = fopen(filename, "r")) == NULL) {
        return false;
    }
    else {
        fclose(fp);
        return true;
    }
}


static void get_backup_filename(const char* filename, uint8_t index, char *backup_name, size_t size) {

    char extension[ROTATING_FILE_EXT_SIZE];

    memcpy(backup_name, filename, size);
    if (index > 0) {
        sprintf(extension, ".%d", index);
        strncat(backup_name, extension, strlen(extension));
    }
}

