#include <stdint.h>
#include <assert.h>
#include <time.h>

#include "platform.h"


/* --------- Local Definitions --------- */
struct timespec {
    uint32_t tv_sec;
    uint32_t tv_nsec;
};

/* --------- Local Variables --------- */
static uint8_t log_mutex;
static uint32_t counter = 0;


/* --------- Public Functions --------- */
void platform_lock_init() {

    log_mutex = 0;
}


void platform_lock() {
    
    assert(log_mutex == 0);
    log_mutex = 1;
}


void platform_unlock() {

    assert(log_mutex == 1);
    log_mutex = 0;
}


struct timespec platform_time() {
    
    struct timespec time = {
        .tv_sec = ++counter,
        .tv_nsec = 0,
    };
    return time;
}