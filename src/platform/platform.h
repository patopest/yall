#ifndef PLATFORM_H
#define PLATFORM_H



void platform_lock_init();
void platform_lock();
void platform_unlock();
struct timespec platform_time();


#endif