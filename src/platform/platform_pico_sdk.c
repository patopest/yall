#include <stdio.h>
#include <time.h>
#include "pico/stdlib.h"
#include <pico/mutex.h>

#include "platform.h"



/* --------- Local Variables --------- */
static mutex_t log_mutex;


/* --------- Public Functions --------- */
void platform_lock_init() {
    
    mutex_init(&log_mutex);
}


void platform_lock() {
    
    mutex_enter_blocking(&log_mutex);
}


void platform_unlock() {
    
    mutex_exit(&log_mutex);
}


struct timespec platform_time() {

    absolute_time_t abs_time = get_absolute_time();
    uint64_t microseconds = to_us_since_boot(abs_time);
    struct timespec time = {
        .tv_sec = microseconds / 1000000,
        .tv_nsec = microseconds % 1000000 * 1000,
    };
    return time;
}

