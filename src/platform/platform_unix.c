#include <stdint.h>
#include <pthread.h>
#include <time.h>

#include "platform.h"


/* --------- Local Variables --------- */
static pthread_mutex_t log_mutex;


/* --------- Public Functions --------- */
void platform_lock_init() {
    
    pthread_mutex_init(&log_mutex, NULL);
}


void platform_lock() {
    
    pthread_mutex_lock(&log_mutex);
}


void platform_unlock() {
    
    pthread_mutex_unlock(&log_mutex);
}


struct timespec platform_time() {

    struct timespec time;
    clock_gettime(CLOCK_REALTIME, &time);
    return time;
}