#include <winsock2.h>
#include <time.h>

#include "platform.h"

/* --------- Local Definitions --------- */
struct timespec {
    uint32_t tv_sec;
    uint32_t tv_nsec;
};


/* --------- Local Variables --------- */
static CRITICAL_SECTION log_mutex;


/* --------- Public Functions --------- */
void platform_lock_init() {

    InitializeCriticalSection(&log_mutex);
}


void platform_lock() {

    EnterCriticalSection(&log_mutex);
}


void platform_unlock() {

    LeaveCriticalSection(&s_mutex);
}



struct timespec platform_time() {
    
    struct timespec time;
    timespec_get(&time, TIME_UTC);

    return time;
}