#ifndef LOGGER_H
#define LOGGER_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdarg.h>


/* --------- Types --------- */
typedef enum {
    LOG_LEVEL_NONE,
    LOG_LEVEL_ERROR,
    LOG_LEVEL_WARN,
    LOG_LEVEL_INFO,
    LOG_LEVEL_DEBUG,
    LOG_LEVEL_VERBOSE
} log_level_t;

typedef int (*vprintf_t)(const char *, va_list);
typedef int (*vfprintf_t)(FILE *, const char *, va_list);

/* --------- Definitions --------- */
#ifndef LOG_ENABLED
#define LOG_ENABLED       (1)
#endif

#ifndef LOG_DEFAULT_LEVEL
#define LOG_DEFAULT_LEVEL LOG_LEVEL_INFO
#endif

#ifndef LOG_MAXIMUM_LEVEL
#define LOG_MAXIMUM_LEVEL LOG_DEFAULT_LEVEL
#endif

#ifndef LOG_LOCAL_LEVEL
#define LOG_LOCAL_LEVEL   LOG_MAXIMUM_LEVEL
#endif

#ifndef LOG_MAX_HANDLERS
#define LOG_MAX_HANDLERS  (3)
#endif


#ifdef LOG_COLOR_OUTPUT
#define LOG_COLOR_BLACK   "30"
#define LOG_COLOR_RED     "31"
#define LOG_COLOR_GREEN   "32"
#define LOG_COLOR_BROWN   "33"
#define LOG_COLOR_BLUE    "34"
#define LOG_COLOR_PURPLE  "35"
#define LOG_COLOR_CYAN    "36"
#define LOG_COLOR_GRAY    "90"
#define LOG_COLOR(COLOR)  "\033[0;" COLOR "m"
#define LOG_BOLD(COLOR)   "\033[1;" COLOR "m"
#define LOG_RESET_COLOR   "\033[0m"
#define LOG_COLOR_E       LOG_COLOR(LOG_COLOR_RED)
#define LOG_COLOR_W       LOG_COLOR(LOG_COLOR_BROWN)
#define LOG_COLOR_I       LOG_COLOR(LOG_COLOR_GREEN)
#define LOG_COLOR_D
#define LOG_COLOR_V       LOG_COLOR(LOG_COLOR_GRAY)
#else
#define LOG_COLOR_E
#define LOG_COLOR_W
#define LOG_COLOR_I
#define LOG_COLOR_D
#define LOG_COLOR_V
#define LOG_RESET_COLOR
#endif

// #define LOG_COLOR_LEVEL(level_char) LOG_COLOR_ ## level_char


// Log Formats
#define LOG_FORMAT_JSON(msg) "{\"level\":\"%s\", \"ts\":\"%s\", \"component\":\"%s\", \"caller\":\"%s:%ld\", \"msg\":\"" msg "\"}\n"
#define LOG_FORMAT_JSON_ARGS(level, level_short, tag, time, file, line, ...) level, (time), tag, file, line, ##__VA_ARGS__

#define LOG_FORMAT_KEYS(msg) "ts=%s caller=%s:%ld level=%s component=%s msg=\"" msg "\"\n"
#define LOG_FORMAT_KEYS_ARGS(level, level_short, tag, time, file, line, ...) (time), file, line, level, tag, ##__VA_ARGS__

#define LOG_FORMAT_DEFAULT(msg) "%c [%s] %s (%s:%ld): " msg "\n"
#define LOG_FORMAT_DEFAULT_ARGS(level, level_short, tag, time, file, line, ...) level_short, (time), tag, file, line, ##__VA_ARGS__

#ifndef LOG_FORMAT
#define LOG_FORMAT      LOG_FORMAT_DEFAULT
#define LOG_FORMAT_ARGS LOG_FORMAT_DEFAULT_ARGS
#endif

// Datetime Formats
#ifndef LOG_TIMESTAMP_FORMAT
#define LOG_TIMESTAMP_FORMAT "%Y-%m-%d %H:%M:%S" // The strftime() format
// #define LOG_TIMESTAMP_FORMAT "%FT%T%z" // RFC3339 format
#endif
#ifndef LOG_TIMESTAMP_FORMAT_SIZE
#define LOG_TIMESTAMP_FORMAT_SIZE  20 // The size of the string for the strftime format
#endif

#ifdef        LOG_TIMESTAMP_MILLISECONDS
#define LOG_TIMESTAMP_SUBSECONDS   1
#elif defined LOG_TIMESTAMP_MICROSECONDS
#define LOG_TIMESTAMP_SUBSECONDS   2
#elif defined LOG_TIMESTAMP_NANOSECONDS
#define LOG_TIMESTAMP_SUBSECONDS   3
#else
#define LOG_TIMESTAMP_SUBSECONDS   0
#endif

#define LOG_TIMESTAMP_SUBSECONDS_SIZE (1 + 3 * LOG_TIMESTAMP_SUBSECONDS) // The size of the string containing the subseconds.
#define LOG_TIMESTAMP_TOTAL_SIZE      LOG_TIMESTAMP_FORMAT_SIZE + LOG_TIMESTAMP_SUBSECONDS_SIZE // Size of the buffer to allocate



#ifdef __FILE_NAME__ // Predefined Macro on GCC >= 12 and Clang > 9
#define __FILENAME__ __FILE_NAME__ 
#else
#define __FILENAME__ __FILE__
#endif


// Macros to output log at predefined level
#define LOGE(tag, format, ...)  LOG(LOG_LEVEL_ERROR,   tag, format, ##__VA_ARGS__)
#define LOGW(tag, format, ...)  LOG(LOG_LEVEL_WARN,    tag, format, ##__VA_ARGS__)
#define LOGI(tag, format, ...)  LOG(LOG_LEVEL_INFO,    tag, format, ##__VA_ARGS__)
#define LOGD(tag, format, ...)  LOG(LOG_LEVEL_DEBUG,   tag, format, ##__VA_ARGS__)
#define LOGV(tag, format, ...)  LOG(LOG_LEVEL_VERBOSE, tag, format, ##__VA_ARGS__)

#define log_e LOGE
#define log_w LOGW
#define log_i LOGI
#define log_d LOGD
#define log_v LOGV
#define log_error   LOGE
#define log_warning LOGW
#define log_info    LOGI
#define log_debug   LOGD
#define log_verbose LOGV

// Macro to output log at specified level
#define LOG(level, tag, format, ...) do { \
    if (LOG_ENABLED && level <= LOG_LOCAL_LEVEL) LOG_LOCAL(level, tag, format, ##__VA_ARGS__); \
    } while(0)

#define log   LOG


// Macro to call log write function
#define LOG_LOCAL(level, tag, format, ...) do {  \
    if      (level == LOG_LEVEL_ERROR)   { logger__write(LOG_LEVEL_ERROR,   tag, LOG_FORMAT(format), LOG_FORMAT_ARGS("error",   'E', tag, logger__timestamp(LOG_TIMESTAMP_FORMAT, LOG_TIMESTAMP_SUBSECONDS), __FILENAME__, __LINE__, ##__VA_ARGS__)); } \
    else if (level == LOG_LEVEL_WARN)    { logger__write(LOG_LEVEL_WARN,    tag, LOG_FORMAT(format), LOG_FORMAT_ARGS("warning", 'W', tag, logger__timestamp(LOG_TIMESTAMP_FORMAT, LOG_TIMESTAMP_SUBSECONDS), __FILENAME__, __LINE__, ##__VA_ARGS__)); } \
    else if (level == LOG_LEVEL_INFO)    { logger__write(LOG_LEVEL_INFO,    tag, LOG_FORMAT(format), LOG_FORMAT_ARGS("info",    'I', tag, logger__timestamp(LOG_TIMESTAMP_FORMAT, LOG_TIMESTAMP_SUBSECONDS), __FILENAME__, __LINE__, ##__VA_ARGS__)); } \
    else if (level == LOG_LEVEL_DEBUG)   { logger__write(LOG_LEVEL_DEBUG,   tag, LOG_FORMAT(format), LOG_FORMAT_ARGS("debug",   'D', tag, logger__timestamp(LOG_TIMESTAMP_FORMAT, LOG_TIMESTAMP_SUBSECONDS), __FILENAME__, __LINE__, ##__VA_ARGS__)); } \
    else if (level == LOG_LEVEL_VERBOSE) { logger__write(LOG_LEVEL_VERBOSE, tag, LOG_FORMAT(format), LOG_FORMAT_ARGS("verbose", 'V', tag, logger__timestamp(LOG_TIMESTAMP_FORMAT, LOG_TIMESTAMP_SUBSECONDS), __FILENAME__, __LINE__, ##__VA_ARGS__)); } \
    } while(0)



/* --------- Functions --------- */
void logger__init();
void logger__enable();
void logger__disable();
bool logger__is_enabled();
void logger__set_level(log_level_t level);
log_level_t logger__get_level();

char *logger__timestamp(const char* format, uint8_t subseconds);
void logger__write(log_level_t level, const char *tag, const char *format, ...);


#include "logger_handlers.h"

void logger__init_handlers();
void logger__remove_handlers();
void logger__flush_handlers();
void logger__handler_set_print_function(const char *name, vfprintf_t print_func);
void logger__handler_set_filename(const char *name, char *filename);

#include "logger_tags.h"



#ifdef __cplusplus
} // extern "C"
#endif


#endif