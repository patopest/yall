#ifndef LOGGER_HANDLERS_H
#define LOGGER_HANDLERS_H


#include "logger.h"


/* --------- Types --------- */
enum logger_handler_type_e {
    LOGGER_HANDLER_NONE,
    LOGGER_HANDLER_CONSOLE,
    LOGGER_HANDLER_FILE,
    LOGGER_HANDLER_ROTATINGFILE,
    LOGGER_HANDLER_OTHER,
};
typedef enum logger_handler_type_e logger_handler_type_t;


struct logger_handler_console_s {
    FILE* file;
    vfprintf_t print;
};
typedef struct logger_handler_console_s logger_handler_console_t;


struct logger_handler_file_s {
    FILE* file;
    vfprintf_t print;
    char *filename;
};
typedef struct logger_handler_file_s logger_handler_file_t;

struct logger_handler_rotating_file_s {
    FILE* file;
    vfprintf_t print;
    char *filename;
    uint32_t file_size;     // in Bytes
    uint32_t max_file_size; // in Bytes
    uint8_t backup_count;
};
typedef struct logger_handler_rotating_file_s logger_handler_rotating_file_t;


 typedef union {
    logger_handler_console_t        console;
    logger_handler_file_t           file;
    logger_handler_rotating_file_t  rotating_file;
} logger_handler_context_t;


struct logger_handler_s {
    // Common attributes
    const char *name;
    logger_handler_type_t type;
    // Required functions
    void (*init)(logger_handler_context_t *ctx); 
    void (*write)(logger_handler_context_t *ctx, const char *format, va_list args);
    void (*flush)(logger_handler_context_t *ctx);
    // Handler specific data
    logger_handler_context_t context;
};
typedef struct logger_handler_s logger_handler_t;



/* --------- Functions --------- */
logger_handler_t logger__handler_get_default();

logger_handler_t logger__handler_get_console(const char *name, FILE *stream);
logger_handler_t logger__handler_get_file(const char *name, char *filename);
logger_handler_t logger__handler_get_rotating_file(const char *name, char *filename, uint32_t max_file_size, uint8_t backup_count);




// Defined here to avoid circular dependencies with logger.h
// Implementation is in logger.c
extern void logger__add_handler(logger_handler_t handler);
extern bool logger__remove_handler(const char* name);
extern logger_handler_t *logger__get_handler(const char *name);







#endif