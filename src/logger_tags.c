#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <string.h>

#include "logger_tags.h"
#include "logger.h"
#include "platform.h"


/*
Current tags implementation is similar to esp_log with linked-list to store all tags
and a binary min-heap cache.
*/


/* --------- Local Defintions --------- */
#define TAG_CACHE_SIZE 31

struct tag_entry_s {
    const char *tag;
    uint32_t level : 3;
    uint32_t counter : 29;
};
typedef struct tag_entry_s tag_entry_t;

struct tags_list_entry_s {
    struct tags_list_entry_s *next;
    log_level_t level;
    char tag[0];    // start of string, will malloc accounting for tag size
};
typedef struct tags_list_entry_s tags_list_entry_t;


/* --------- Local Variables --------- */
static tags_list_entry_t *head = NULL;
static tag_entry_t cache[TAG_CACHE_SIZE];
static uint32_t cache_num_entries = 0;
static uint32_t max_counter = 0;
static log_level_t tag_default_level = LOG_DEFAULT_LEVEL;



/* --------- Local Functions --------- */
static void list_add(const char *tag, log_level_t level);
static void list_insert(tags_list_entry_t *entry);
static tags_list_entry_t* list_find(const char *tag);
static void list_clear();
static void cache_add(const char *tag, log_level_t level);
static tag_entry_t* cache_find(const char *tag);
static void cache_clear();
static void cache_bubble_down(uint8_t index);

static void print_cache();





void logger__set_tag_level(const char *tag, log_level_t level) {

    platform_lock();

    // Wildcard: remove all linked list items and clear cache
    if (strcmp("*", tag) == 0) {
        list_clear();
        cache_clear();
        tag_default_level = level;
        platform_unlock();
        return;
    }

    // Find existing tag in list
    tags_list_entry_t *it = list_find(tag);
    if (it == NULL) { // Create tag entry in list
        list_add(tag, level);
    }
    else { // entry exists
        it->level = level;
    }

    // Update cache entry
    tag_entry_t *ct = cache_find(tag);
    if (ct != NULL) {
        ct->level = level;
    }

    platform_unlock();
}


log_level_t logger__get_tag_level(const char *tag) {
    log_level_t tag_level = LOG_LEVEL_NONE;

    platform_lock();

    // Get tag in cache
    tag_entry_t *ce = cache_find(tag);
    if (ce != NULL) {
        tag_level = ce->level;
    }
    else { // Get tag in list
        tags_list_entry_t *le = list_find(tag);
        if (le != NULL) {
            tag_level = le->level;
        }
        else {
            tag_level = tag_default_level;
            list_add(tag, tag_level);
        }
        cache_add(tag, tag_level);
    }

    platform_unlock();

    return tag_level;
}




/* --------- Linked List Functions --------- */
static void list_add(const char *tag, log_level_t level) {

    size_t tag_len = strlen(tag) + 1; // account for '\0' char
    tags_list_entry_t *new = (tags_list_entry_t *) malloc(offsetof(tags_list_entry_t, tag) + tag_len);
    if (!new) {
        fprintf(stderr, "Error: Unable to create new log tag list entry. Call to malloc failed\n");
        return;
    }
    new->level = level;
    memcpy(new->tag, tag, tag_len);
    list_insert(new);
}


static void list_insert(tags_list_entry_t *entry) {

    entry->next = head;
    head = entry;
}


static tags_list_entry_t* list_find(const char *tag) {

    tags_list_entry_t *le = head;
    if (le == NULL) {
        return le;
    }
    for (; le->next != NULL; le = le->next) {
        if (strcmp(le->tag, tag) == 0){
            break;
        }
    }

    return le;
}


static void list_clear() {

    if (head == NULL) {
        return;
    }
    while (head->next != NULL) {
        tags_list_entry_t *tmp = head;
        head = head->next;
        free(tmp);
    }
    free(head);
    head = NULL;
}


/* --------- Cache Heap Functions --------- */
static void cache_add(const char *tag, log_level_t level) {

    uint32_t counter = max_counter++;
    tag_entry_t new = {
        .tag = tag,
        .level = level,
        .counter = counter,
    };

    // Cache is not filled up yet
    if (cache_num_entries < TAG_CACHE_SIZE) {
        cache[cache_num_entries++] = new;
    }
    // Cache is full: replace oldest entry (head)
    else {
        cache[0] = new;
        cache_bubble_down(0);
    }
}


static tag_entry_t* cache_find(const char *tag) {

    tag_entry_t *entry = NULL;
    for(uint8_t i = 0; i < cache_num_entries; i++) {
        if (cache[i].tag == tag) {
            entry = &cache[i];
            break;
        }
    }

    return entry;
}


static void cache_clear() {

    memset(cache, 0x00, sizeof(cache));
    cache_num_entries = 0;
    max_counter = 0;

}


static void cache_bubble_down(uint8_t index) {

    while (index < TAG_CACHE_SIZE / 2) {
        uint8_t left_child = index * 2 + 1;
        uint8_t right_child = left_child + 1;
        // Choose which child to swap entry with 
        uint8_t child = right_child;
        if (cache[left_child].counter < cache[right_child].counter) {
            child = left_child;
        }
        // Swap parent and child
        tag_entry_t tmp = cache[index];
        cache[index] = cache[child];
        cache[child] = tmp;

        // Continue to traverse
        index = child;
    }
}


static void print_cache() {

    for (uint8_t i = 0; i < cache_num_entries; i++) {
        uint8_t left_child = i * 2 + 1;
        uint8_t right_child = left_child + 1;

        printf("node (cnt:%d): %s\n", cache[i].counter, cache[i].tag);
        if (left_child < cache_num_entries)
            printf("\tL child (cnt:%d): %s\n", cache[left_child].counter, cache[left_child].tag);
        if (right_child < cache_num_entries)
            printf("\tR child (cnt:%d): %s\n", cache[right_child].counter, cache[right_child].tag);
    }

}








