#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "logger.h"
#include "nanounit.h"


// static int return_code = 0;

// // Dummy printf function to check that the call to vprintf happens
// static int test_printf(FILE *file, const char *str, va_list arg) {
//     return_code = 1;
//     return 0;
// }



static int test_single_handler() {

    // test default handler is present
    logger_handler_t *handler = logger__get_handler("console");
    nu_assert(handler);

    // test removal
    bool remove = logger__remove_handler("console");
    nu_assert(remove);
    handler = logger__get_handler("console");
    nu_assert_eq_ptr(NULL, handler);

    // add it back
    logger__add_handler(logger__handler_get_console("console", NULL));
    handler = logger__get_handler("console");
    nu_assert(handler);

    return 0;
}


static int test_multi_handlers() {

    // add a second handler
    logger_handler_t new = logger__handler_get_console("console2", stdout);
    logger__add_handler(new);
    logger_handler_t *handler = logger__get_handler("console2");
    nu_assert(handler);

    // add third handler
    new = logger__handler_get_console("console3", stdout);
    logger__add_handler(new);
    handler = logger__get_handler("console3");
    nu_assert(handler);

    // remove second handler and check others still exist
    bool remove = logger__remove_handler("console2");
    nu_assert(remove);
    handler = logger__get_handler("console");
    nu_assert(handler);
    handler = logger__get_handler("console2");
    nu_assert_eq_ptr(NULL, handler);
    handler = logger__get_handler("console3");
    nu_assert(handler);

    return 0;
}


int main(int argc, char* argv[]) {

    logger__init();

    nu_run_test(test_single_handler);
    nu_run_test(test_multi_handlers);

    nu_report();
}
