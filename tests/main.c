#include <stdio.h>

#define LOG_LOCAL_LEVEL LOG_LEVEL_VERBOSE

#define LOG_FORMAT      LOG_FORMAT_KEYS
#define LOG_FORMAT_ARGS LOG_FORMAT_KEYS_ARGS

#include "logger.h"


#include <unistd.h>


void test_tag() {
    
    log_i("test", "hello again");
    logger__set_tag_level("test", LOG_LEVEL_ERROR);
    log_w("test", "hello again");
    log_e("test", "hello again");

}


int main(int argc, char *argv[]) {

    printf("hello\n");

    const char *tag = "main";

    logger__init();
    logger__set_level(LOG_LEVEL_VERBOSE);
    // logger__set_tag_level("main", LOG_LEVEL_VERBOSE);
    // logger__add_handler(logger__handler_get_rotating_file());
    logger__init();

    log(LOG_LEVEL_VERBOSE, "main", "hello");
    log_d("main", "hello");
    // sleep(5);
    log_i("main", "hello");

    // printf("%2$d, %1$d, %3$d %s\n", 1, 2, 3, "hello");
    log_i("main", "%d, %d, %d %s", 1, 2, 3, "hello");



    test_tag();
}