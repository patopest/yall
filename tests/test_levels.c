#include <stdio.h>

#define LOG_DEFAULT_LEVEL LOG_LEVEL_VERBOSE
#include "logger.h"
#include "nanounit.h"



static const char *tag = "test";
static int return_code = 0;

// Dummy printf function to check that the call to vprintf happens
static int test_printf(FILE* file, const char *str, va_list arg) {
    return_code = 1;
    return 0;
}



static int test_enable() {

    logger__set_level(LOG_LEVEL_INFO);

    logger__disable();
    nu_assert(!logger__is_enabled());
    return_code = 0;
    log_i(tag, "Info");
    nu_assert_eq_int(0, return_code); // Should not print

    logger__enable();
    nu_assert(logger__is_enabled());
    return_code = 0;
    log_i(tag, "Info");
    nu_assert(return_code); // Should print

    return 0;
}


static int test_verbose() {
    /* when: */
    logger__set_level(LOG_LEVEL_VERBOSE);
    logger__set_tag_level(tag, LOG_LEVEL_VERBOSE);

    /* then: */
    nu_assert_eq_int(LOG_LEVEL_VERBOSE, logger__get_level());
    return_code = 0;
    log_e(tag, "Error");
    nu_assert(return_code); // Should print

    return_code = 0;
    log_w(tag, "Warning");
    nu_assert(return_code); // Should print
    
    return_code = 0;
    log_i(tag, "Info");
    nu_assert(return_code); // Should print
    
    return_code = 0;
    log_d(tag, "Debug");
    nu_assert(return_code); // Should print
    
    return_code = 0;
    log_v(tag, "Verbose");
    nu_assert(return_code); // Should print

    return 0;
}


static int test_debug() {
    /* when: */
    logger__set_level(LOG_LEVEL_DEBUG);
    logger__set_tag_level(tag, LOG_LEVEL_VERBOSE);

    /* then: */
    nu_assert_eq_int(LOG_LEVEL_DEBUG, logger__get_level());
    return_code = 0;
    log_e(tag, "Error");
    nu_assert(return_code); // Should print

    return_code = 0;
    log_w(tag, "Warning");
    nu_assert(return_code); // Should print
    
    return_code = 0;
    log_i(tag, "Info");
    nu_assert(return_code); // Should print
    
    return_code = 0;
    log_d(tag, "Debug");
    nu_assert(return_code); // Should print
    
    return_code = 0;
    log_v(tag, "Verbose");
    nu_assert_eq_int(0, return_code); // Should not print

    return 0;
}


static int test_info() {
    /* when: */
    logger__set_level(LOG_LEVEL_INFO);
    logger__set_tag_level(tag, LOG_LEVEL_VERBOSE);

    /* then: */
    nu_assert_eq_int(LOG_LEVEL_INFO, logger__get_level());
    return_code = 0;
    log_e(tag, "Error");
    nu_assert(return_code); // Should print

    return_code = 0;
    log_w(tag, "Warning");
    nu_assert(return_code); // Should print
    
    return_code = 0;
    log_i(tag, "Info");
    nu_assert(return_code); // Should print
    
    return_code = 0;
    log_d(tag, "Debug");
    nu_assert_eq_int(0, return_code); // Should not print
    
    return_code = 0;
    log_v(tag, "Verbose");
    nu_assert_eq_int(0, return_code); // Should not print

    return 0;
}


static int test_warn() {
    /* when: */
    logger__set_level(LOG_LEVEL_WARN);
    logger__set_tag_level(tag, LOG_LEVEL_VERBOSE);

    /* then: */
    nu_assert_eq_int(LOG_LEVEL_WARN, logger__get_level());
    return_code = 0;
    log_e(tag, "Error");
    nu_assert(return_code); // Should print

    return_code = 0;
    log_w(tag, "Warning");
    nu_assert(return_code); // Should print
    
    return_code = 0;
    log_i(tag, "Info");
    nu_assert_eq_int(0, return_code); // Should not print
    
    return_code = 0;
    log_d(tag, "Debug");
    nu_assert_eq_int(0, return_code); // Should not print
    
    return_code = 0;
    log_v(tag, "Verbose");
    nu_assert_eq_int(0, return_code); // Should not print

    return 0;
}


static int test_error() {
    /* when: */
    logger__set_level(LOG_LEVEL_ERROR);
    logger__set_tag_level(tag, LOG_LEVEL_VERBOSE);

    /* then: */
    nu_assert_eq_int(LOG_LEVEL_ERROR, logger__get_level());
    return_code = 0;
    log_e(tag, "Error");
    nu_assert(return_code); // Should print

    return_code = 0;
    log_w(tag, "Warning");
    nu_assert_eq_int(0, return_code); // Should not print
    
    return_code = 0;
    log_i(tag, "Info");
    nu_assert_eq_int(0, return_code); // Should not print
    
    return_code = 0;
    log_d(tag, "Debug");
    nu_assert_eq_int(0, return_code); // Should not print
    
    return_code = 0;
    log_v(tag, "Verbose");
    nu_assert_eq_int(0, return_code); // Should not print

    return 0;
}


int main(int argc, char* argv[]) {

    logger__init();
    logger__handler_set_print_function("console", test_printf);

    nu_run_test(test_enable);
    nu_run_test(test_verbose);
    nu_run_test(test_debug);
    nu_run_test(test_info);
    nu_run_test(test_warn);
    nu_run_test(test_error);

    nu_report();
}
