#include <stdio.h>
#if defined(_WIN32) || defined(_WIN64)
#include <io.h>
#else
#include <fcntl.h>
#include <unistd.h>
#endif

#if defined(_WIN32) || defined(_WIN64)
#define dup _dup
#define dup2 _dup2
#define fileno _fileno
#endif

// Overriding default log format for simpler test asserts
#define LOG_FORMAT(msg) "%c" msg "\n"
#define LOG_FORMAT_ARGS(level, level_short, tag, time, file, line, ...) level_short, ##__VA_ARGS__

#include "logger.h"
#include "nanounit.h"


/* --------- Local Variables --------- */
static const char *filename = "console.log";



static int test_console() {

    const char *tag = "test";
    const char *message = "test message";
    int stdoutfd;
    FILE* redirect;
    FILE* fp;
    char line[256];
    int count = 0;

    remove(filename);

    // redirect stdout to file
    stdoutfd = dup(STDOUT_FILENO);
    if ((redirect = fopen(filename, "w")) == NULL) {
        nu_fail();
    }
    dup2(fileno(redirect), STDOUT_FILENO);

    // init logger with console handler
    logger_handler_t new = logger__handler_get_console("console", stdout);
    logger__add_handler(new);
    logger__init();
    logger_handler_t *handler = logger__get_handler("console");
    nu_assert(handler);

    // log
    LOGI(tag, "%s", message);
    logger__flush_handlers();

    // test
    if ((fp = fopen(filename, "r")) == NULL) {
        nu_fail();
    }
    while (fgets(line, sizeof(line), fp) != NULL) {
        line[strlen(line) - 1] = '\0'; // remove LF
        nu_assert_eq_int('I', line[0]);
        nu_assert_eq_str(message, &line[strlen(line) - strlen(message)]);
        count++;
    }
    nu_assert_eq_int(1, count);

    // cleanup: restore original stdout
    dup2(stdoutfd, STDOUT_FILENO);
    fclose(fp);
    fclose(redirect);
    close(stdoutfd);
    remove(filename);
    
    return 0;
}


int main(int argc, char* argv[]) {

    nu_run_test(test_console);

    nu_report();
}
