#include <stdio.h>
#include <stdlib.h>

// Overriding default log format for simpler test asserts
#define LOG_FORMAT(msg) "%c" msg "\n"
#define LOG_FORMAT_ARGS(level, level_short, tag, time, file, line, ...) level_short, ##__VA_ARGS__

#include "logger.h"
#include "nanounit.h"



// Helper functions
static void get_backup_filename(const char* filename, uint8_t index, char *backup_name, size_t size) {

    char extension[5];

    memcpy(backup_name, filename, size);
    if (index > 0) {
        sprintf(extension, ".%d", index);
        strncat(backup_name, extension, strlen(extension));
    }
}


// Helper function to read a log file and compare with a defined message
static int log_file_comparator(const char *filename, const char *message, const char level_char) {

    int count = 0;
    char line[256];
    FILE *fp;

    if ((fp = fopen(filename, "r")) == NULL) {
        nu_fail();
    }
    while (fgets(line, sizeof(line), fp) != NULL) {
        line[strlen(line) - 1] = '\0'; // remove LF
        nu_assert_eq_int(level_char, line[0]);
        nu_assert_eq_str(message, &line[strlen(line) - strlen(message)]);
        count++;
    }
    fclose(fp);

    return count;
}



static int test_simple_file() {

    char *filename = "file.log";
    const char *tag = "test";
    const char *message = "test message";

    remove(filename);

    // init logger with rotating file handler
    logger_handler_t new = logger__handler_get_rotating_file("rotating_file", filename, 100000, 10);
    logger__remove_handlers();
    logger__add_handler(new);
    logger__init();
    logger_handler_t *handler = logger__get_handler("rotating_file");
    nu_assert(handler);

    // log
    LOGI(tag, "%s", message);
    logger__flush_handlers();

    // test
    int count = log_file_comparator(filename, message, 'I');
    nu_assert_eq_int(1, count);

    // cleanup
    remove(filename);
    
    return 0;
}


static int test_rotating_file() {

    const char *tag = "test";
    const char *message = "test message";
    char *filename = "rotating.log";
    int count = 0;

    remove(filename);

    // init logger with rotating file handler
    // with parameters to trigger rotation after first log
    int max_file_size = strlen(message);
    logger_handler_t new = logger__handler_get_rotating_file("rotating_file", filename, max_file_size, 3);
    logger__remove_handlers();
    logger__add_handler(new);
    logger__init();
    logger_handler_t *handler = logger__get_handler("rotating_file");
    nu_assert(handler);

    // Log 1rst line
    LOGI(tag, "%s", message); // should rotate after printing message
    logger__flush_handlers();

    // Test backup "*.log.1" file : should contain previous log line
    int size = strlen(message) + 5;
    char *backup_filename = (char *) malloc(size);
    get_backup_filename(filename, 1, backup_filename, size);
    count = log_file_comparator(backup_filename, message, 'I');
    nu_assert_eq_int(1, count);

    // Test backup "*.log" main file : should not contain previous log line
    count = log_file_comparator(filename, message, 'I');
    nu_assert_eq_int(0, count);


    // Log 2nd line
    LOGE(tag, "%s", message); // should rotate after printing message
    logger__flush_handlers();

    // Test backup "*.log.1" file : should contain previous log line
    get_backup_filename(filename, 1, backup_filename, size);
    count = log_file_comparator(backup_filename, message, 'E');
    nu_assert_eq_int(1, count);

    // Test backup "*.log.2" file : should contain first log line
    get_backup_filename(filename, 2, backup_filename, size);
    count = log_file_comparator(backup_filename, message, 'I');
    nu_assert_eq_int(1, count);

    // Test backup "*.log" main file : should not contain anything
    count = log_file_comparator(filename, message, 'I');
    nu_assert_eq_int(0, count);

    // cleanup
    remove(filename);
    free(backup_filename);
    
    return 0;
}


int main(int argc, char* argv[]) {

    nu_run_test(test_simple_file);
    nu_run_test(test_rotating_file);

    nu_report();
}
