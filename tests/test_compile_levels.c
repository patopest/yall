#include <stdio.h>

#include "logger.h"
#include "nanounit.h"


static const char* tag = "test";
static int return_code = 0;

// Dummy printf function to check that the call to vprintf happens
static int test_printf(FILE *file, const char* str, va_list arg) {
    return_code = 1;
    return 0;
}



static int test_enabled() {

    logger__set_level(LOG_LEVEL_INFO);

    #undef  LOG_ENABLED
    #define LOG_ENABLED (0)

    return_code = 0;
    log_i(tag, "Info");
    nu_assert_eq_int(0, return_code); // Should not print

    #undef  LOG_ENABLED
    #define LOG_ENABLED (1)

    return_code = 0;
    log_i(tag, "Info");
    nu_assert(return_code); // Should print

    return 0;
}


static int test_compile_level() {

    logger__set_level(LOG_LEVEL_VERBOSE);
    logger__set_tag_level(tag, LOG_LEVEL_VERBOSE);

    #undef  LOG_LOCAL_LEVEL
    #define LOG_LOCAL_LEVEL  LOG_LEVEL_INFO

    return_code = 0;
    log_i(tag, "Info");
    nu_assert(return_code); // Should print
    
    return_code = 0;
    log_d(tag, "Debug");
    nu_assert_eq_int(0, return_code); // Should not print
    
    return_code = 0;
    log_v(tag, "Verbose");
    nu_assert_eq_int(0, return_code); // Should not print


    #undef  LOG_LOCAL_LEVEL
    #define LOG_LOCAL_LEVEL  LOG_LEVEL_DEBUG

    return_code = 0;
    log_i(tag, "Info");
    nu_assert(return_code); // Should print
    
    return_code = 0;
    log_d(tag, "Debug");
    nu_assert(return_code); // Should print
    
    return_code = 0;
    log_v(tag, "Verbose");
    nu_assert_eq_int(0, return_code); // Should not print

    return 0;
}



int main(int argc, char* argv[]) {

    logger__init();
    logger__handler_set_print_function("console", test_printf);

    nu_run_test(test_enabled);
    nu_run_test(test_compile_level);

    nu_report();
}
