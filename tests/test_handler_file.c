#include <stdio.h>

// Overriding default log format for simpler test asserts
#define LOG_FORMAT(msg) "%c" msg "\n"
#define LOG_FORMAT_ARGS(level, level_short, tag, time, file, line, ...) level_short, ##__VA_ARGS__

#include "logger.h"
#include "nanounit.h"



static int test_file() {

    char *filename = "file.log";
    const char *tag = "test";
    const char *message = "test message";
    FILE* fp;
    char line[256];
    int count = 0;

    remove(filename);

    // init logger with file handler
    logger_handler_t new = logger__handler_get_file("file", filename);
    logger__add_handler(new);
    logger__init();
    logger_handler_t *handler = logger__get_handler("file");
    nu_assert(handler);

    // log
    LOGI(tag, "%s", message);
    logger__flush_handlers();

    // test
    if ((fp = fopen(filename, "r")) == NULL) {
        nu_fail();
    }
    while (fgets(line, sizeof(line), fp) != NULL) {
        line[strlen(line) - 1] = '\0'; // remove LF
        nu_assert_eq_int('I', line[0]);
        nu_assert_eq_str(message, &line[strlen(line) - strlen(message)]);
        count++;
    }
    nu_assert_eq_int(1, count);

    // cleanup
    fclose(fp);
    remove(filename);
    
    return 0;
}


int main(int argc, char* argv[]) {

    nu_run_test(test_file);
    nu_report();
}
