#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "logger.h"
#include "nanounit.h"


static const char* tag = "test";
static int return_code = 0;
static int cache_size = 31;

// Dummy printf function to check that the call to vprintf happens
static int test_printf(FILE *file, const char *str, va_list arg) {
    return_code = 1;
    return 0;
}



static int test_tag_level() {

    logger__set_tag_level(tag, LOG_LEVEL_ERROR);
    nu_assert_eq_int(LOG_LEVEL_ERROR, logger__get_tag_level(tag));

    return_code = 0;
    LOGE(tag, "Error");
    nu_assert(return_code); // Should print

    return_code = 0;
    LOGW(tag, "Warning");
    nu_assert_eq_int(0, return_code); // Should not print

    return 0;
}


static int test_global_level() {

    logger__set_level(LOG_LEVEL_WARN);
    logger__set_tag_level(tag, LOG_LEVEL_WARN);
    nu_assert_eq_int(LOG_LEVEL_WARN, logger__get_level());
    nu_assert_eq_int(LOG_LEVEL_WARN, logger__get_tag_level(tag));

    return_code = 0;
    LOGW(tag, "Warning");
    nu_assert(return_code); // Should print

    logger__set_level(LOG_LEVEL_ERROR);
    return_code = 0;
    LOGW(tag, "Warning");
    nu_assert_eq_int(0, return_code); // Should not print

    logger__set_tag_level(tag, LOG_LEVEL_DEBUG);
    return_code = 0;
    LOGI(tag, "Info");
    nu_assert_eq_int(0, return_code); // Should not print (master_level < tag_level)

    logger__set_level(LOG_LEVEL_DEBUG);
    return_code = 0;
    LOGD(tag, "Debug");
    nu_assert(return_code); // Should print now

    return 0;
}


static int test_tag_cache() {

    const char *base_tag = tag;
    int run_for = cache_size + 5;

    logger__set_level(LOG_LEVEL_INFO);

    for (uint8_t i = 0; i <= run_for; i++) {
        // Creating unique tag
        size_t tag_len = strlen(base_tag);
        char *tag = malloc(tag_len + 3);
        memcpy(tag, base_tag, tag_len);
        sprintf(&tag[tag_len], "%d", i);

        return_code = 0;
        LOGI(tag, "test");
        nu_assert(return_code); // Should print
    }

    return 0;
}



int main(int argc, char* argv[]) {

    logger__init();
    logger__handler_set_print_function("console", test_printf);

    nu_run_test(test_tag_level);
    nu_run_test(test_global_level);
    nu_run_test(test_tag_cache);

    nu_report();
}
