#ifndef FORMAT_H
#define FORMAT_H


// Custom format
#define LOG_FORMAT(msg) "%s [%s] %s: " msg "\n"
#define LOG_FORMAT_ARGS(level, level_short, tag, time, file, line, ...) (time), level, tag, ##__VA_ARGS__

// Custom timestamp format (RFC3339 compliant)
#define LOG_TIMESTAMP_FORMAT "%FT%T"
#define LOG_TIMESTAMP_MICROSECONDS


#include "logger.h" // include after overriding options


#endif