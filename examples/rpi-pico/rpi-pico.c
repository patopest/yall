#include <stdio.h>
#include "pico/stdlib.h"
#include "hardware/gpio.h"

#include "logger.h"


/* --------- Local Variables --------- */
const char* tag = "main";
const uint LED_PIN = PICO_DEFAULT_LED_PIN;



int main(int argc, char* argv[]) {
    
    stdio_init_all();

    logger__init();
    logger__set_level(LOG_LEVEL_DEBUG);
    log_info(tag, "Hello World!");

    gpio_init(LED_PIN);
    gpio_set_dir(LED_PIN, GPIO_OUT);
    uint led_state = 0;
    while (true) {
        if (led_state == 0) {
            led_state = 1;
        }
        else {
            led_state = 0;
        }
        log_debug(tag, "Putting led to %d", led_state);
        gpio_put(LED_PIN, led_state);
        sleep_ms(1000);
    }
}