#include "logger.h"


char* filename = "out.log";


int main(int argc, char* argv[]) {
    
    const char* tag = "rotating";
    uint32_t max_file_size = 10; // in Bytes, small size to see rotation faster
    uint8_t backup_count = 10;

    logger_handler_t handler = logger__handler_get_rotating_file("rotating", filename, max_file_size, backup_count);
    logger__add_handler(handler);
    logger__init();

    log_info(tag, "Hello World!");

    log_error(tag, "Bye World!");
}