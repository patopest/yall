#include "logger.h"


char* filename = "out.log";


int main(int argc, char* argv[]) {
    
    const char* tag = "multi";

    logger_handler_t handler = logger__handler_get_console("console", stdout);
    logger__add_handler(handler);
    logger_handler_t handler2 = logger__handler_get_file("file", filename);
    logger__add_handler(handler2);
    logger__init();

    log_info(tag, "Hello World!");
}