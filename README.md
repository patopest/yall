# Yet Another Logging Library

A C logging library for both Software and Firmware.


## Features

TODO


## Usage

See [Documentation](https://yall.bambinito.net)


## Build

### Use [task](https://taskfile.dev/)

```shell
task default
```

- See available tasks:

```shell
task --list-all
```

### Manual commands

Same as what the [Taskfile](./Taskfile.yml) does.

- Setup build directory

```shell
cmake -S . -B build
```

- Build

```shell
cmake --build build
```

- Run Tests

```shell
cmake -Dbuild_tests=ON -DCMAKE_BUILD_TYPE=Debug -S . -B build
ctest -T Test --test-dir build
```

- Generate coverage report

Requires `gcov` and `gcovr`.

```shell
cmake -Dbuild_tests=ON -Dbuild_coverage=ON -DCMAKE_BUILD_TYPE=Debug -S . -B build
ctest -T Test -T Coverage --test-dir build
```

Open report file in browser of choice

```shell
open ./build/coverage/index.html
```


## Building examples

The `build_examples` option need to be set during configuration.

```shell
cmake -Dbuild_examples=ON -S . -B build
```

Compile specific examples using the `--target` property:

```shell
cmake --build build --target getting-started
```

Run the target example:

```shell
./build/examples/getting-started/getting-started
```

### RPI Pico example

The example cannot be build with the rest of the examples due to the library needing to be build with the [Pico SDK](https://github.com/raspberrypi/pico-sdk).

It can be built as a separate project in the [example's directory(./examples/rpi-pico)] directly.

```shell
cd examples/rpi-pico
cmake -S . -B build
cmake --build build
```



## Documentation

Uses [MkDocs](https://www.mkdocs.org/) with [Material for MkDocs theme](https://squidfunk.github.io/mkdocs-material/).

- Setup environment

```shell
cd docs
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
```

- Develop

```shell
mkdocs server
```


